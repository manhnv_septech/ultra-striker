const Roulette = (function(){

    var pushAudio = null;
    var rotationAudio = null;
    var rotationSlowDownAudio = null;
    var rotationSingleAudio = null;
    var backwardAudio = null;
    var spinAgainAudio = null;

    const spinSpeed = 50;
    const spinAngleDefault = 18;
    var spinTimer = {};

    var angle = 0;

    const Roulette = function(id, prizes, audioPlayer){
        this.id = id;
        this.prizes = prizes;
        this.panel = new RoulettePanel();

        this.isSpinning = false;
        this.isButtonEnable = true;

        pushAudio = audioPlayer.source('push');
        rotationAudio = audioPlayer.source('rotation');
        rotationSlowDownAudio = audioPlayer.source('rotationSlowDown');
        rotationSingleAudio = audioPlayer.source('rotationSingle');
        backwardAudio = audioPlayer.source('back');
        spinAgainAudio = audioPlayer.source('spinAgain');
    }
    const p = Roulette.prototype;

    p.init = function(){
        angle = 0;
        $("#roulettePanelImg").rotate(angle);
        rotationAudio.currentTime = 0;
        rotationSlowDownAudio.currentTime = 0;
        backwardAudio.currentTime = 0;
        spinAgainAudio.currentTime = 0;
    }

    p.start = function(){
        this.isSpinning = true;
        pushAudio = audioPlayer.source('push');
        pushAudio.currentTime = 0;
        pushAudio.start();
        pushAudio.currentTime = 0;
        rotationAudio = audioPlayer.source('rotation');
        rotationAudio.start();
        rotationAudio.loop = true;
        this.spin(spinAngleDefault);
    }

    p.spin = function(spinAngle) {
        spinTimer = setInterval(function(){
            angle+=spinAngle;
            $("#roulettePanelImg").rotate(angle);
        }, spinSpeed);
    }

    p.slowDown = function(spinAngle, callback){
        slowDownRecursion(spinAngle, callback);
    }

    function slowDownRecursion(spinAngle, callback){
        spinTimer = setInterval(function(){
            clearInterval(spinTimer);
            angle+=spinAngle;
            $("#roulettePanelImg").rotate(angle);
            spinAngle === 2.25 ? callback() : slowDownRecursion(spinAngle - 0.25, callback);

        }, spinSpeed);
    }

    p.stopTo = function(roulette, stopAngle, callback){
        return function() {
            clearInterval(spinTimer);
            const currentAngle = (angle % 360 - 360)
            const reverseDistance = Math.ceil(currentAngle / 90) * 90
            const stopAngleOnPanel = stopAngle + reverseDistance
            angle = currentAngle;
            rotationSlowDownAudio.stop();
            spinTimer = setInterval(function(){
                if(angle === stopAngleOnPanel){
                    clearInterval(spinTimer);
                    setTimeout(function() {
                        callback();
                    }, 1000);
                } else {
                    const angleRatio = 1.125;
                    angle += angleRatio;
                    if(angle % 22.5 === 0){
                        rotationSingleAudio = audioPlayer.source('rotationSingle');
                        rotationSingleAudio.start();
                    }
                    $("#roulettePanelImg").rotate(angle);
                }
            }, spinSpeed);
        }
    }

            p.centrifugalForce = function(prize, sorryAngle, callback){

                const pointerAngle = function(){
                    switch(prize.category){
                        case 'parka':
                            return 45
                        case 'battery':
                            return 22.5
                        case 'pcBag':
                            return 90
                        default:
                            return 0
                    }
                }

                const spinAll = function(){
                    $("#roulette").rotate({
                        duration:11000,
                        angle: angle,
                        animateTo: 10800,
                        easing: $.easing.easeInOutCirc,
                        callback: function(){
                            setTimeout(function(){
                            }, 4000);
                        }
                    });
                };

                const spin = function(){
                    $("#roulettePanelImg").rotate({
                        duration:5000,
                        angle: angle,
                        animateTo: 2160,
                        callback: function(){
                            $('#rouletteHitZone').fadeOut();
                            $('#roulettePointer').rotate({
                                duration:1,
                                angle: 0,
                                animateTo: pointerAngle()
                            });
                            $('#rouletteHitZone').rotate({
                                duration:1,
                                angle: 0,
                                animateTo: pointerAngle()
                            });
                        }
                    });
                };

                spin();
                spinAll();

                $('#roulette').addClass('roulette-shrink');
                $('#ultraStrikerText').addClass('centrifugal-force-ultra-striker');
                $('#hitText').addClass('centrifugal-force-hit');
                $('#roulettePointerImg').addClass('centrifugal-force-arrow');

                setTimeout(function(){
                    $('#rouletteHitZone').show();
                    $('#rouletteHitZone').addClass('hitZoneBlink');
                    callback();
                }, 15000);

            }

            p.ultraSpecium = function(targetAngle, callback){
                const spin = function(){
                    $("#roulettePanelImg").rotate({
                        duration:5000,
                        angle: angle,
                        animateTo: targetAngle + 2160,
                        callback: function(){
                            callback();
                        }
                    });
                };

                const video = $('#ultraTransform');
                video.get(0).currentTime = 0;
                video.show();
                video.get(0).play();
                video.on('ended',function() {
                    video.fadeOut();
                    video.get(0).currentTime = 0;
                    $('#specium').show();
                    $('#speciumImg').addClass('speciumAppear');
                    setTimeout(function(){
                        $('#speciumBeam').addClass('specium-beam-shake');
                        $('#speciumBeam').show();
                        setTimeout(function(){
                            spin();
                        },200);
                        setTimeout(function(){
                            $('#speciumBeam').hide();
                            $('#speciumImg').removeClass('speciumAppear');
                            $('#speciumImg').addClass('speciumDisAppear');
                            setTimeout(function(){
                                $('#specium').hide();
                                $('#speciumImg').removeClass('speciumDisAppear');
                            }, 2500);
                        },1000);
                    }, 3500)
                });
            }

            p.allParka = function(targetAngle, callback){

                const fadeIn = function(){
                    $('#roulettePanelImg').attr('src', './img/all_parka.png');
                    $('#rouletteHitZone').hide();
                    $('#roulettePointerImg').hide();
                    $('#hit').hide();
                    $('#ultra-striker').addClass('ultraZoomUp');
                };

                const spin = function(){
                    $("#roulettePanelImg").rotate({
                        duration:14000,
                        angle: angle,
                        animateTo: targetAngle + 4320,
                        callback: function(){
                            hit();
                        }
                    });
                };

                const hit = function(){
                    $('#roulettePointerImg').css({top: -500, left: 1000});
                    $('#roulettePointerImg').show();
                    $('#hit').css({top: -1000, left: 500});
                    $('#hit').show();
                    $("#hit").animate({top: 0, left: 0}, 300);
                    $("#roulettePointerImg").animate({top: 0, left: 0}, 300, function(){
                        $('#rouletteHitZone').show();
                        $('#rouletteHitZone').addClass('hitZoneBlink');
                        setTimeout(callback, 2000);
                    });

                };

                spin();
                $('#ultra-striker').addClass('textZoomOutUp');
                $('#hit').addClass('textZoomOutUp');
                $('#rouletteEnclosure').addClass('panelZoomOutUp');
                setTimeout(function(){
                    fadeIn();
                }, 4000);
                setTimeout(function(){
                    $('#rouletteEnclosure').removeClass('panelZoomOutUp');
                    $('#ultra-striker').removeClass('textZoomOutUp');
                    $('#hit').removeClass('textZoomOutUp');
                }, 8000);
            }

            p.dragForward = function(num, callback){
                const drag = function(num){
                    return function(){
                        if(num === 0) {
                            $("#roulettePointerImg").animate({top: 0, left: 0}, 300);
                            callback();
                            return;
                        }
                        $("#roulettePointerImg").rotate({
                            duration: 300,
                            angle: 0,
                            center: ["90%", "20%"],
                            animateTo: -20,
                            callback: function(){
                                rotationSingleAudio = audioPlayer.source('rotationSingle');
                                rotationSingleAudio.start();
                                $("#roulettePointerImg").rotate({
                                    duration: 300,
                                    angle: -20,
                                    center: ["90%", "20%"],
                                    animateTo: 0,
                                    callback: function(){
                                        return;
                                    }
                                });
                            }
                        });
                        $("#roulettePanelImg").rotate({
                            duration: 600,
                            angle: angle,
                            animateTo: angle += 22.5,
                            callback: function(){
                                drag(num - 1)();
                            }
                        });
                    }
                }

                const prepareDrag = function(){
                    const dragStartPositionTop = $("#roulettePointerImg").height() * -0.25;
                    const dragStartPositionLeft = $("#roulettePointerImg").width() * -0.25;
                    $("#roulettePointerImg").animate(
                        {top: dragStartPositionTop, left: dragStartPositionLeft},
                        1000,
                        'linear',
                        function(){setTimeout(drag(num), 500);}
                    );
                }
                prepareDrag();
            }

            p.spinBackward = function(num, callback){
                const backward = function(){
                    setTimeout(function(){
                        $("#roulettePointerImg").rotate({
                            duration: 100,
                            angle: 30,
                            center: ["90%", "20%"],
                            animateTo: -30,
                            callback: function(){
                                setTimeout(function(){
                                    $("#roulettePointerImg").rotate({
                                        duration: 1000,
                                        angle: -30,
                                        center: ["90%", "20%"],
                                        animateTo: 0,
                                        callback: function(){

                                        }
                                    });
                                    $("#roulettePointerImg").animate({top: 0, left: 0}, 1000, 'linear');
                                }, 300);
                            }
                        });
                        $("#roulettePanelImg").rotate({
                            duration: 3000,
                            angle: angle,
                            animateTo: angle + ((22.5 * num) - 270),
                            callback: function(){
                                backwardAudio.stop();
                                callback();
                            }
                        });
                        backwardAudio = audioPlayer.source('back');
                        backwardAudio.start();
                    }, 300);
                }

                const prepareSpin = function(){
                    const dragStartPositionTop = $("#roulettePointerImg").height() * 0.4;
                    const dragStartPositionLeft = $("#roulettePointerImg").width() * -0.7;
                    $("#roulettePointerImg").animate(
                        {top: dragStartPositionTop, left: dragStartPositionLeft},
                        1000,
                        'linear',
                        function() {
                            setTimeout(function () {
                                $("#roulettePointerImg").rotate({
                                    duration: 300,
                                    angle: 0,
                                    center: ["90%", "20%"],
                                    animateTo: 30,
                                    callback: backward
                                });
                            }, 500);
                        });
                }
                prepareSpin();

            }

            p.spinToHit = function(targetAngle, callback){

                const spin = function(){
                    spinAgainAudio = audioPlayer.source('spinAgain');
                    spinAgainAudio.start();
                    pushAudio = audioPlayer.source('push');
                    pushAudio.start();
                    $("#roulettePanelImg").rotate({
                        duration:4000,
                        angle: angle,
                        animateTo: targetAngle + 1080,
                        callback: function(){
                            callback();
                        }
                    });
                }

                const topToCenter = $("#roulettePointerImg").height() * -0.05;
                const leftToCenter = $("#roulettePointerImg").width() * -0.26;

                const pressButton = function(onComplete){
                    return function(){
                        $("#roulettePointerImg").animate(
                            {top: topToCenter + $("#roulettePointerImg").height() * 0.05, left: leftToCenter},
                            100,
                            'linear',
                            function(){
                                $("#roulettePointerImg").animate({top: topToCenter, left: leftToCenter}, 100, 'linear', function(){
                                    $('#rouletteButtonImg').attr('src', './img/ultra_03.png');
                                    onComplete();
                                });
                            }
                        );
                        setTimeout(function(){
                            $('#rouletteButtonImg').attr('src', './img/ultra_05.png');
                        }, 30);
                    }
                }

                const moveToCenter = function(onComplete){
                    $("#roulettePointerImg").animate({top: topToCenter, left: leftToCenter}, 1000, 'linear', function(){
                        setTimeout(onComplete, 1000);
                    });
                    $("#roulettePointerImg").rotate({
                        duration: 1500,
                        angle: 0,
                        center: ["70%", "50%"],
                        animateTo: -75,
                        callback: function () {}
                    });
                }

                const backToDefaultPlace = function(atSameTime){
                    return function(){
                        setTimeout(function(){
                            $("#roulettePointerImg").animate({top: 0, left: 0}, 1000, 'linear');
                            $("#roulettePointerImg").rotate({
                                duration: 1500,
                                angle: -75,
                                center: ["70%", "50%"],
                                animateTo: 0,
                                callback: function(){}
                            });
                        }, 500);
                        atSameTime();
                    }
                }

                moveToCenter(pressButton(backToDefaultPlace(spin)));


            }

            p.stop = function(acquiredPrize, callback){
                this.isButtonEnable = false;
                rotationAudio.stop();
                rotationSlowDownAudio = audioPlayer.source('rotationSlowDown');
                rotationSlowDownAudio.currentTime = 1;
                rotationSlowDownAudio.start();
                clearInterval(spinTimer);

                const hitAction = drawHitAction(this, acquiredPrize, this.panel, callback)

                this.slowDown(spinAngleDefault, this.stopTo(this, hitAction.firstStopAngle, hitAction.action));
            }

            function drawHitAction(roulette, acquiredPrize, panel, callback){
                const hitAngle = panel.angleOf(acquiredPrize.category);
                const isHitSorry = acquiredPrize.category === 'sorry';
                const isHitParka = acquiredPrize.category === 'parka';
                const sorryAngle = panel.angleOf('sorry');
                const distance = (hitAngle - sorryAngle) / panel.anglePerOneElm();
                const afterHitAction = function(wait){
                    return function(){
                        setTimeout(function(){
                            callback();
                            roulette.isSpinning = false;
                            roulette.isButtonEnable = true;
                        }, wait);
                    }
                }
                const sorryAction = function(){
                    setTimeout(function(){
                        $('#ultraStrikerLogo').hide();
                        afterHitAction(1500)();
                    }, 1500);
                }
                const forwardAction = function(){
                    setTimeout(function(){
                        roulette.dragForward(distance, afterHitAction(2000));
                    }, 1500);
                }
                const backWordAction = function(){
                    setTimeout(function(){
                        roulette.spinBackward(distance - 4, afterHitAction(2000));
                    }, 1500);
                }
                const spinToHitAction = function(){
                    setTimeout(function(){
                        roulette.spinToHit(hitAngle, afterHitAction(2000));
                    }, 1500);
                }
                const allParkaAction = function(){
                    logoBlink(function(){
                        $('#ultraStrikerLogo').fadeOut();
                        roulette.allParka(hitAngle, afterHitAction(2000));
                    }, true);
                }
                const ultraSpeciumAction = function(){
                    logoBlink(function(){
                        $('#ultraStrikerLogo').fadeOut();
                        roulette.ultraSpecium(hitAngle, afterHitAction(2000));
                    }, true);
                }
                const centrifugalForceAction = function(){
                    logoBlink(function(){
                        $('#ultraStrikerLogo').fadeOut();
                        roulette.centrifugalForce(acquiredPrize, sorryAngle, afterHitAction(2000));
                    }, true);
                }

                const logoBlink = function(callback, isWin){
                    $('#ultraStrikerLogo').addClass('ultra-striker-logo-blink');
                    $('#ultraStrikerLogo').show();
                    setTimeout(function(){
                        $('#ultraStrikerLogo').removeClass('ultra-striker-logo-blink');

                        if(isWin){
                            $('#ultraStrikerLogo img').addClass('ultra-striker-logo-shake');
                            setTimeout(function(){
                                callback();
                            }, 2000);
                        } else {
                            callback();
                        }
                    }, 3000);
                }

                const random = Math.floor( Math.random() * 7);
                if(isHitSorry){
                    return new HitAction(hitAngle, sorryAction);
                } else if(isHitParka && random == 0){
                    return new HitAction(sorryAngle, allParkaAction);
                } else {
                    switch (random){
                        case 1:
                            return new HitAction(sorryAngle, centrifugalForceAction);
                        case 2:
                            return new HitAction(sorryAngle, ultraSpeciumAction);
                        default :
                            return new HitAction(hitAngle, afterHitAction(1500));
                    }
                }
    }

    return Roulette;
})();

const HitAction = (function(){
    const HitAction = function(firstStopAngle, action){
        this.firstStopAngle = firstStopAngle;
        this.action = action;
    }
    const p = HitAction.prototype;

    return HitAction;
})();

const RoulettePanel = (function(){
    const RoulettePanel = function(){
        this.items = [
            new RoulettePanelItem('pcBag', calculateAngle(16, 3)),
            new RoulettePanelItem('battery', calculateAngle(16, 2)),
            new RoulettePanelItem('parka', calculateAngle(16, 1)),
            new RoulettePanelItem('sorry', calculateAngle(16, 0)),
        ];
        this.itemNum = 16;

    }
    const p = RoulettePanel.prototype;

    p.angleOf = function(category){
        const roulettePanelItem = this.items.find(function(item){ return item.key === category; });
        return roulettePanelItem.angle;
    }

    p.anglePerOneElm = function(){
        return 360 / this.itemNum;
    }

    function calculateAngle(totalItemNumber, index){
        return 360 / totalItemNumber * (index + 1);
    }

    return RoulettePanel;
})();

const RoulettePanelItem = (function(){
    const RoulettePanelItem = function(key, angle){
        this.key = key;
        this.angle = angle;
    }
    const p = RoulettePanelItem.prototype;

    return RoulettePanelItem;
})();