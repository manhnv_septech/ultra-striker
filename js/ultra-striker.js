$.blockUI({ message: '<h1>Now Loading...</h1>' });
var audioPlayer = null;

$(window).on('load', function(){

    checkStoreEnabled();

    audioPlayer = new AudioPlayer(startGame);

    var fanfareAudio = $('#fanfareAudio')[0];
    var booAudio = $('#booAudio')[0];
    var bgmAudio = null;

    var prizeRepository = new PrizeRepository();;
    var roulette = null;

    var isReadyToPlay = true;

    function start() {
        roulette.init();
        isReadyToPlay = false;
        bgmAudio.stop();
        roulette.start();
        $('#startMessage').hide();
        $('#stopMessage').hide();
        $('.setting-link').hide();
        setTimeout(function(){
            if(roulette.isButtonEnable){
                $('#stopMessage').show();
            }
        }, 2000);
    }

    function stop() {
        const prizes =  prizeRepository.resolveAll();
        const lottery = new Lottery(prizes);
        const drawResult = lottery.draw();
        $('#startMessage').hide();
        $('#stopMessage').hide();

        roulette.stop(drawResult.acquiredPrize, showHitPrize(drawResult.acquiredPrize));
        prizeRepository.storeAll(drawResult.remainingPrizes);
    }

    function showHitPrize(acquiredPrize){
        return function(){
            $('#' + acquiredPrize.id + 'Hit').on('hidden.bs.modal', function () {
                $('.setting-link').show();
                $('#startMessage').show();
            });
            $('#' + acquiredPrize.id + 'Hit').modal();
            const hitAudio = audioPlayer.source(chooseFanfare(acquiredPrize))
            hitAudio.start();
            setTimeout(function(){
                if(!roulette.isSpinning){
                    bgmAudio.currentTime = 0;
                    bgmAudio = audioPlayer.source('bgm');
                    bgmAudio.loop = true;
                    bgmAudio.start();
                }
                isReadyToPlay = true;
            }, 5000);
        }
    }

    chooseFanfare = function(acquiredPrize){
        if(acquiredPrize.category == 'sorry'){
            return 'boo';
        } else {
            return 'fanfare';
        }
    }

    function alertEmptyStock(){
        $('#logo').modal('hide');
        showModal('modalBox', '賞品のストックがありません。賞品を補充してください。', '設定画面へ', './setting.html');
    }

    function init(){
        $('#roulettePanelImg').attr('src', './img/gyakkyou_02.png');
        $('#rouletteButtonPushed').hide();
        $('#rouletteButton').show();
        $('#roulettePointer').rotate({
            duration:1,
            angle: 0
        });
        $('#rouletteHitZone').rotate({
            duration:1,
            angle: 0
        });
        $('#roulette').removeClass('roulette-shrink');
        $('#ultraStrikerText').removeClass('centrifugal-force-ultra-striker');
        $('#hitText').removeClass('centrifugal-force-hit');
        $('#roulettePointerImg').removeClass('centrifugal-force-arrow');
    }

    $('#pushZoneImg').on('click', function(){
        if(roulette.isButtonEnable){
            roulette.isSpinning ? stop() : checkStock(prizeRepository.resolveAll(), start, alertEmptyStock);
        }
    });

    $('#pushZoneImg').on('mousedown touchstart', function(){
        if(roulette.isButtonEnable) {
            $('#rouletteButton').hide();
            $('#rouletteButtonPushed').show();
        }
    });

    $('#pushZoneImg').on('mouseup touchend', function(){
        if(roulette.isButtonEnable) {
            $('#rouletteButtonPushed').hide();
            $('#rouletteButton').show();
        }
    });
    $('#pushZoneImg').on('mousemove', function(){
        if(roulette.isButtonEnable) {
            $('#rouletteButtonPushed').hide();
            $('#rouletteButton').show();
        }
    });

    $('.prize').on('click', function(e){
        $('.prize').modal('hide');
        init();
    });

    $('.setting-link').on('click', function(){
        if(roulette.isButtonEnable) {
            location.href = "setting.html";
        }
    });

    function startGame(){
        roulette = new Roulette('rouletteItems', prizeRepository.resolveAll(), audioPlayer);

        $.unblockUI();

        $('#logo').on('hidden.bs.modal', function () {
            $('#startMessage').show();
            $('#stopMessage').hide();
        });
        $('#logo').modal();
        $('#logo').on('click', function(){
            bgmAudio = audioPlayer.source('bgm');
            bgmAudio.loop = true;
            bgmAudio.start();
        })
        checkStock(prizeRepository.resolveAll(),init , alertEmptyStock);
    }

    //bgmAudio.play();
    //bgmAudio.loop = true;


});