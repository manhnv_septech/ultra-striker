const Lottery = (function(){

    const Lottery = function(prizes) {
        this.prizes = prizes;
    }
    const p = Lottery.prototype;

    p.draw = function(){
        const prizeTotalAmount = this.prizes
            .map(function(prize){ return prize.amount; })
            .reduce(function(a, b){ return a + b; });
        const random = Math.random() * prizeTotalAmount;

        function lottery(prizes, totalCount, index) {
            const acquired = prizes[index];
            const total = acquired.amount + totalCount;
            return (random < total && acquired.amount) ? acquired : lottery(prizes, total, index + 1);
        }
        const acquiredPrize = lottery(this.prizes, 0, 0);
        const remainingPrizes = this.prizes
            .map(function(prize){ return (prize.id === acquiredPrize.id) ? prize.changeAmount(prize.amount - 1) : prize });

        return new DrawResult(remainingPrizes, acquiredPrize);
    }

    return Lottery;
})();

const DrawResult = (function(){
    const DrawResult = function(remainingPrizes, acquiredPrize) {
        this.remainingPrizes = remainingPrizes;
        this.acquiredPrize = acquiredPrize;
    }
    const p = DrawResult.prototype;

    return DrawResult;
})();

