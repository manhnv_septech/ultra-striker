window.AudioContext = window.AudioContext || window.webkitAudioContext;
const context = new AudioContext();
const AudioPlayer = (function(){

    const AudioPlayer = function(callback) {
        this.bufferList = null;
        this.isLoaded = false;
        const bufferLoader = new BufferLoader(
            context,
            {
                back: './audio/back.mp3',
                bgm: './audio/bgm.mp3',
                boo: './audio/boo.mp3',
                fanfare: './audio/Fanfare.mp3',
                push: './audio/push.mp3',
                rotation: './audio/rotation.mp3',
                rotationSlowDown: './audio/rotation_sd.mp3',
                rotationSingle: './audio/rotation_single.mp3',
                spinAgain: './audio/spin_again.mp3',
            },
            this.finishedLoading(callback, this)
        );

        bufferLoader.load();
    }
    const p = AudioPlayer.prototype;

    p.finishedLoading = function(callback, player){
        return function(bufferList){
            player.isLoaded = true;
            player.bufferList = bufferList
            callback();
        };
    }

    p.source = function(key){
        const source = context.createBufferSource();
        source.buffer = this.bufferList[key];
        source.connect(context.destination);
        return source;
    }

    return AudioPlayer;
})();

function BufferLoader(context, urlList, callback) {
    this.context = context;
    this.urlList = urlList;
    this.onload = callback;
    this.bufferList = {};
    this.loadCount = 0;
}

BufferLoader.prototype.loadBuffer = function(url, key) {
    // Load buffer asynchronously
    var request = new XMLHttpRequest();
    request.open("GET", url, true);
    request.responseType = "arraybuffer";

    var loader = this;

    request.onload = function() {
        loader.context.decodeAudioData(
            request.response,
            function(buffer) {
                if (!buffer) {
                    alert('error decoding file data: ' + url);
                    return;
                }
                loader.bufferList[key] = buffer;
                if (++loader.loadCount == Object.keys(loader.urlList).length)
                    loader.onload(loader.bufferList);
            },
            function(error) {
                console.error('decodeAudioData error', error);
            }
        );
    }

    request.onerror = function() {
        alert('BufferLoader: XHR error');
    }

    request.send();
}

BufferLoader.prototype.load = function() {
    const urlList = this.urlList
    const loader = this
    Object.keys(urlList).forEach(function(key){
        loader.loadBuffer(urlList[key], key);
    })
    for (var i = 0; i < this.urlList.length; ++i)
        this.loadBuffer(this.urlList[i], i);
}

