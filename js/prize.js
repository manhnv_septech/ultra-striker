const Prize = (function(){
    const Prize = function(id, name, amount, category) {
        this.id = id;
        this.name = name;
        this.amount = parseInt(amount);
        this.category = category;
    }
    const p = Prize.prototype;

    p.changeAmount = function(amount){
        return new Prize(this.id, this.name, amount, this.category);
    }

    return Prize;
})();

const PrizeRepository = (function(){

    const StoreKey = 'prizes';

    const defaultPrizes = [
        new Prize('parka', '', 0, 'parka'),
        new Prize('pcBag', '', 0, 'pcBag'),
        new Prize('battery', '', 0, 'battery'),
        new Prize('sorry', '', 0, 'sorry')
    ];

    const PrizeRepository = function() {
    }
    const p = PrizeRepository.prototype;

    p.resolveAll = function() {
        const storedPrizes = store.get(StoreKey);
        return storedPrizes ? storedPrizes.map(function(prize){ return new Prize(prize.id, prize.name, prize.amount, prize.category); }) : defaultPrizes;
    }

    p.storeAll = function(prizes) {
        store.set(StoreKey, prizes);
    }

    return PrizeRepository;
})();