$(function(){

    checkStoreEnabled();

    const prizeRepository = new PrizeRepository();

    function alertEmptyStock(){
        showMessage('modalBox', '賞品数が0です。賞品を補充してください。');
    }

    function loadSettings() {
        const prizes = prizeRepository.resolveAll();

        prizes.forEach(function(prize) {
            $('#' + prize.id + 'Amount').val(prize.amount);
        });
    }

    function loadInput() {
        return prizeRepository.resolveAll().map(function(prize) {
            const inputAmount = $('#' + prize.id + 'Amount').val();
            return prize.changeAmount(inputAmount ? inputAmount : 0);
        });
    }

    function storeSettings(updatedPrizes) {
        return function(){
            prizeRepository.storeAll(updatedPrizes);
            showModal('modalBox', '賞品数設定を保存しました。', 'ルーレット画面へ', './ultra-striker.html');
        }
    }

    $('#saveSettings').on('click', function(){
        const inputted = loadInput();
        checkStock(inputted, storeSettings(inputted), alertEmptyStock)
    });

    $('input').numeric({
        decimal: false,
        negative: false
    });

    loadSettings();
});