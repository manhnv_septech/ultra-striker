chrome.browserAction.onClicked.addListener(
    function(){
        const selfId = chrome.i18n.getMessage('@@extension_id');
        chrome.tabs.getSelected( function(tab){
            chrome.tabs.create({
                index:tab.index+1,
                url:'chrome-extension://' + selfId + '/ultra-striker.html',
                selected:true
            });
        });
    }
);