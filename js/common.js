const checkStock = function(prizesToCheck, ifHas, ifDoesNotHave) {
    const prizes = prizesToCheck;
    const prizeTotalAmount = prizes
        .map(function(prize){ return prize.amount; })
        .reduce(function(a, b){ return a + b; });

    (prizeTotalAmount > 0) ? ifHas() : ifDoesNotHave();
};

const checkStoreEnabled = function(){
    if (!store.enabled) {
        showMessage('modalBox', 'ローカルストレージを有効にしてください。');
    }
}

const showMessage = function(id, content){
    showModal(id, content, '', '');
}

const showModal = function(id, content, buttonText, buttonHref){
    $('#' + id).on('shown.bs.modal', function(){
        $('#modalBody').html(content);
        if(buttonText !== ''){
            $('#modalButton').html(buttonText);
            $('#modalLink').attr('href', buttonHref)
        } else {
            $('#modalLink').hide();
        }
    });
    const a = $('#modalBox')
    $('#modalBox').modal();
}